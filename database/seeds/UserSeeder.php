<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::where('email','jashdoshi24@gmail.com')->get()->first();
        if(!$user)
        {
            \App\User::create([
                'name'=>'Jash Doshi',
                'email'=>'jashdoshi24@gmail.com',
                'password'=>\Illuminate\Support\Facades\Hash::make('jashdoshi'),
                'role'=>'admin'
            ]);
        }else{
            $user->update(['role'=>'admin']);
        }

        \App\User::create([
            'name'=>'Shreyal Doshi',
            'email'=>'shreyal@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('shreyaldoshi'),
            
        ]);
        \App\User::create([
            'name'=>'Vijay Doshi',
            'email'=>'vijay@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('vijaydoshi'),
            
        ]);
    }
}
